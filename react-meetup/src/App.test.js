import "@testing-library/react/dont-cleanup-after-each";
import { render, screen, cleanup } from "@testing-library/react";
import App from "./App";

describe('<App/>', () => {
  afterAll(() => {
    cleanup();
  });

  test("renders App without crashing", () => {
    render(<App/>);
  });
  
  test("renders the navigation component", () => {
    expect(screen.getByTestId('navigation-header')).toBeInTheDocument();
  });
  
  test("renders the Layout component", () => {
    expect(screen.getByTestId('layout')).toBeInTheDocument();
  });

  test("fetches the meetups", async () => {
    //expect(screen.getByText("Loading...")).toBeInTheDocument();

    /*await waitFor(() => 
      expect(screen.findAllByTestId('meetup-item')).toBeGreaterThan(0)
    );*/
  })
});