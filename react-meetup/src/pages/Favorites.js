import MeetupItem from "../components/meetups/MeetupItem";
import classes from "../components/meetups/MeetupList.module.css";
import { useAppContext } from "../util-hooks/useAppContext";

export default function FavoritesPage() {
  const [state] = useAppContext();
  return (
    <section>
      <h1>Favorites Page</h1>
      <ul className={classes.list}>
        {state.favorites.map((item) => (
          <MeetupItem key={item.id} item={item} />
        ))}
      </ul>
    </section>
  );
}
