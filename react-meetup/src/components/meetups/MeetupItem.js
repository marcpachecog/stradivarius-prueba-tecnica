import classes from "./MeetupItem.module.css";
import Card from "../ui/Card";
import { useState } from "react";
import { ACTION_TYPE } from "../../actionTypes";
import { useAppContext } from "../../util-hooks/useAppContext";

export default function MeetupItem({ item }) {
  const [state, dispatch] = useAppContext();
  const [isFavorite, setIsFavorite] = useState(
    state.favorites.find((favorite) => favorite.id === item.id)
  );

  const handleFavoriteClick = () => {
    if (state.favorites.find((favorite) => favorite.id === item.id)) {
      dispatch({ type: ACTION_TYPE.REMOVE_FROM_FAVORITES, payload: item });
    } else {
      dispatch({ type: ACTION_TYPE.ADD_TO_FAVORITES, payload: item });
    }
    setIsFavorite(!isFavorite);
  };

  return (
    <li className={classes.item} data-testid="meetup-item">
      <Card>
        <div className={classes.image}>
          <img src={item.image} alt={item.title} />
        </div>
        <div className={classes.content}>
          <h3>{item.title}</h3>
          <address>{item.address}</address>
          <p>{item.description}</p>
        </div>
        <div className={classes.actions}>
          <button onClick={() => handleFavoriteClick()} data-testid="favorites-button">
            {isFavorite ? "Remove from favorites" : "Add to favorites"}
          </button>
        </div>
      </Card>
    </li>
  );
}
