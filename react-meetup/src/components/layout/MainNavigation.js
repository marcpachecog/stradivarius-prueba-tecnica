import classes from "./MainNavigation.module.css";

import { NavLink } from "react-router-dom";
import { ROUTES } from "../../routes";
import { useEffect, useState } from "react";
import cx from "classnames";
import {
  DIRECTION,
  useScrollYDirection,
} from "../../util-hooks/useScrollYDirection";
import { useAppContext } from "../../util-hooks/useAppContext";

const NavItem = ({ to, badgeCount, children, ...props }) => {
  return (
    <li>
      <NavLink
        to={to}
        className={({ isActive }) => (isActive ? classes.active : undefined)}
        {...props}
      >
        {children}
        {(badgeCount > 0) && <span className={classes.badge} data-testid="favorites-badge">{badgeCount}</span>}
      </NavLink>
    </li>
  );
};

export default function MainNavigation() {
  const [state] = useAppContext();

  const direction = useScrollYDirection();
  const [isHeaderHidden, setIsHeaderHidden] = useState(false);

  useEffect(() => {
    console.log();
    setIsHeaderHidden(direction === DIRECTION.DOWN);
  }, [direction]);

  return (
    <header
      className={cx(classes.header, { [classes.hidden]: isHeaderHidden })}
      data-testid="navigation-header"
    >
      <div className={classes.logo}>React Meetups</div>
      <nav>
        <ul>
          <NavItem to={ROUTES.ALL_MEETUPS}>All Meetups</NavItem>
          <NavItem to={ROUTES.NEW_MEETUP}>Add New Meetup</NavItem>
          <NavItem to={ROUTES.FAVORITES} badgeCount={state.favorites.length} data-testid="navlink-favorites">
            My Favorites
          </NavItem>
        </ul>
      </nav>
    </header>
  );
}
