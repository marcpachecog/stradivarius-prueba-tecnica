import AllMeetupsPage from "./pages/AllMeetupsPage";
import FavoritesPage from "./pages/Favorites";
import NewMeetupsPage from "./pages/NewMeetup";
import MainNavigation from "./components/layout/MainNavigation";
import Layout from "./components/layout/Layout";
import { ROUTES } from "./routes";
import { Route, Routes, Router, BrowserRouter } from "react-router-dom";
import React from "react";
import AppContext from "./AppContext";
import useStore from "./store";

function App() {
  const store = useStore();
  return (
    <BrowserRouter>
      <AppContext.Provider value={store}>
        <div data-testid="app">
          <MainNavigation />
          <Layout>
            <Routes>
              <Route path={ROUTES.ALL_MEETUPS} element={<AllMeetupsPage />} />
              <Route path={ROUTES.NEW_MEETUP} element={<NewMeetupsPage />} />
              <Route path={ROUTES.FAVORITES} element={<FavoritesPage />} />
            </Routes>
          </Layout>
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
