import { useCallback, useEffect, useState } from "react";

export const DIRECTION = {
  UP: 0,
  DOWN: 1,
};

export const useScrollYDirection = () => {
  const [direction, setDirection] = useState(null);

  let prevScrollY = window.scrollY;

  const handleScroll = useCallback(() => {
    const currentScrollY = window.scrollY;
    setDirection(prevScrollY < currentScrollY ? DIRECTION.DOWN : DIRECTION.UP);
    prevScrollY = currentScrollY;
  });

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll]);
  
  return direction;
};
