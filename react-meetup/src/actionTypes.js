export const ACTION_TYPE = {
    ADD_TO_FAVORITES: 'addToFavorites',
    REMOVE_FROM_FAVORITES: 'removeFromFavorites',
};