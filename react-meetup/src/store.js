import { useReducer } from "react";
import { ACTION_TYPE } from "./actionTypes";

const initialState = { favorites: [] };

function favoritesReducer(state, action) {
  switch (action.type) {
    case ACTION_TYPE.ADD_TO_FAVORITES:
      return {
        favorites: [...state.favorites, action.payload],
      };
    case ACTION_TYPE.REMOVE_FROM_FAVORITES:
      return {
        favorites: state.favorites.filter(
          (favorite) => favorite.id !== action.payload.id
        ),
      };
    default:
      throw new Error();
  }
}

export default function useStore() {
  return useReducer(favoritesReducer, initialState);
}
