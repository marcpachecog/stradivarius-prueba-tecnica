const ROUTES = {
  ALL_MEETUPS: "/",
  FAVORITES: "favorites",
  NEW_MEETUP: "new",
};

export { ROUTES };
