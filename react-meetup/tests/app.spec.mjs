import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test('fetches and renders all the meetups', async ({ page }) => { 
  expect(await page.locator('data-testid=meetup-item').count()).toBeGreaterThan(0);
});

test('shows the meetups in the favorites page when meetup is added to favorites', async ({ page }) => {
  await page.locator('data-testid=favorites-button').first().click();
  await page.mouse.wheel(0, -10);
  await page.locator('data-testid=navlink-favorites').click();
  expect(page.locator('data-testid=meetup-item')).toBeTruthy();
});

test('updates the badge when meetup is added to favorites', async ({ page }) => {
  const favoriteButton = page.locator('data-testid=favorites-button').first();

  await favoriteButton.click();
  await expect(page.locator('data-testid=favorites-badge')).toHaveText("1");
  await favoriteButton.click();
  await expect(page.locator('data-testid=favorites-badge')).not.toBeVisible();
});

test('favorite button displays the correct text', async ({ page }) => {
  const favoriteButton = page.locator('data-testid=favorites-button').first();

  await expect(favoriteButton).toHaveText("Add to favorites");
  await favoriteButton.click();
  await expect(favoriteButton).toHaveText("Remove from favorites");
});

test('hides and shows the header when scroll', async ({ page }) => {
  const header = page.locator('data-testid=navigation-header');
  await page.mouse.wheel(0, 10);
  await expect(header).toBeVisible();
  await page.mouse.wheel(0, -10);
  await expect(header).toBeVisible();
});
