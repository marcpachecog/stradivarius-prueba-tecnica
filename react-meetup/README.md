# Solución prueba técnica "react-meetup"

### Navegación

Se ha utilizado react-router-dom

### Manejo del estado

Al tratarse de una app pequeña, en vez de instalar redux junto con redux toolkit. Se ha utilizado directamente los hooks useReducer junto con useContext para definir un estado global accesible a todos los componentes de la app. 

### `npm install`

Instalar todos los paquetes del package.json.

### `npm start`

Ejecutar la app en modo desarrollo.

### `npm test`

Ejecutar los tests de react-testing-library

### `npx playwright test`

Ejecutar los tests e2e de playwright