# Solución prueba técnica primer enunciado

Le he dado varias vueltas pero con la información dada, no acabo de ver dónde está la posible fragilidad en el código, necesitaría algo más de contexto.
No sé si os referís a implementar algún tipo de patrón de diseño estructural para cumplir con los principios SOLID.

Agradecería que me enviáseis la solución del ejercicio.